#!/bin/bash

# gpg --list-keys --keyid-format LONG
# gpg --armor --export keyid

# configure git
git config --global user.name "Roni Carvalho"
git config --global user.email roni.carvalho@encoders.com.br
git config --global user.signingkey keyid
git config --global commit.gpgsign true
git config --global tag.gpgsign true
git config --global init.defaultbranch main
