#!/bin/bash

# pop-os update
sudo apt update -y

# remove software that I dont like
sudo apt remove -y libreoffice*

# install locate
sudo apt install -y plocate mlocate

# install gnome tools
sudo apt install -y gnome-tweaks gnome-extensions-app

# install dev tools
sudo apt install -y git-flow just cargo python-is-python3 python3.10-venv

# install dotnet dev
sudo apt install -y dotnet-sdk-* dotnet-templates-*

# install java
sudo apt install -y openjdk-17-jdk openjdk-21-jdk openjdk-21-testsupport

# install low level dev tools
sudo apt install -y fpc lazarus nasm xxd qemu gnucobol

# install libs for flutter dev
sudo apt install clang cmake ninja-build

# install virtual machines
sudo apt install -y virt-manager

# install go
bash ./setup_go.sh

# install node
bash ./setup_node.sh

# install jetbrains
bash ./setup_jetbrains.sh

# install docker
bash ./setup_docker.sh

# install oh-my-posh
bash ./setup_posh.sh

# install sdkman
bash ./setup_sdkman.sh

# install gnome extensions
bash ./setup_extensions.sh

# configure git
bash ./setup_git.sh

# install flatpak
sudo apt install -y flatpak

# add flatpak source
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# update flatpak repos
sudo flatpak update -y

# flatpak tools
bash ./setup_flatpak.sh

echo "Setup completed"

read -v 1 -s -r -p "Press any key to restart"

echo "Restarting the System"

sudo reboot

