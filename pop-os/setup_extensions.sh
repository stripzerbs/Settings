#!/bin/bash

# install pano
gnome-extensions install https://extensions.gnome.org/extension-data/panoelhan.io.v22.shell-extension.zip

# install transparent top bar
gnome-extensions install https://extensions.gnome.org/extension-data/transparent-top-barftpix.com.v19.shell-extension.zip
