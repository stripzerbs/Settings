#!/bin/bash

# install flatpak programs
flatpak install -y flathub com.google.Chrome
flatpak install -y flathub com.microsoft.EdgeDev
flatpak install -y flathub com.brave.Browser
flatpak install -y flathub org.inkscape.Inkscape
flatpak install -y flathub org.videolan.VLC
flatpak install -y flathub org.gimp.GIMP
flatpak install -y flathub com.jgraph.drawio.desktop
flatpak install -y flathub com.obsproject.Studio
flatpak install -y flathub com.spotify.Client
flatpak install -y flathub rest.insomnia.Insomnia
flatpak install -y flathub net.epson.epsonscan2
flatpak install -y flathub net.kvirc.KVIrc
flatpak install -y flathub org.telegram.desktop
flatpak install -y flathub com.github.IsmaelMartinez.teams_for_linux
flatpak install -y flathub com.discordapp.Discord
flatpak install -y flathub com.github.dail8859.NotepadNext
flatpak install -y flathub org.kde.krita
flatpak install -y flathub org.onlyoffice.desktopeditors
flatpak install -y flathub org.exbin.BinEd
flatpak install -y flathub com.protonvpn.www
flatpak install -y flathub org.apache.jmeter
flatpak install -y flathub com.mongodb.Compass
