#!/bin/bash

wget https://go.dev/dl/go1.22.6.linux-amd64.tar.gz

sudo tar -C /usr/local -xzf go1.22.6.linux-amd64.tar.gz

echo ''
echo '# added by setup_go'
echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.profile

source ~/.profile

rm go1.22.6.linux-amd64.tar.gz
