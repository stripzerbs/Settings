#!/bin/bash

# install ohmyposh
curl -s https://ohmyposh.dev/install.sh | bash -s

source ~/.profile

# install fonts
bash setup_fonts.sh

# variables to install themes
poshthemes_url=https://github.com/JanDeDobbeleer/oh-my-posh/releases/download/v19.18.1/themes.zip
poshthemes=~/.poshthemes

# directory to themes
if [ ! -d "$poshthemes" ]; then
  mkdir "$poshthemes"
fi

# install themes
curl -L -o themes.zip "$poshthemes_url" && unzip -q themes.zip -d "$poshthemes" && rm themes.zip

# set ohmyposh
echo 'eval "$(oh-my-posh init bash --config ~/.poshthemes/montys.omp.json)"' >> ~/.bashrc


