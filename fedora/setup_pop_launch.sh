#!/bin/bash

# clone pop-shell repo
git clone https://github.com/pop-os/launcher.git

# workdir
cd launcher

# build and install
just build-release && {

  just install

  cd ..

  rm -rf launcher

} || {
  echo "failure to build launcher"
}
