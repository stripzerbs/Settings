#!/bin/bash

# add microsoft repo
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo

# update repository
sudo dnf check-update -y

# install vscode
sudo dnf install -y code
