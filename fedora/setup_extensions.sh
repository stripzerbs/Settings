#!/bin/bash

# install user themes
gnome-extensions install https://extensions.gnome.org/extension-data/user-themegnome-shell-extensions.gcampax.github.com.v54.shell-extension.zip

# install azwallpaper
gnome-extensions install https://extensions.gnome.org/extension-data/azwallpaperazwallpaper.gitlab.com.v8.shell-extension.zip

# install move clock
gnome-extensions install https://extensions.gnome.org/extension-data/Move_Clockrmy.pobox.com.v29.shell-extension.zip

# install tray icons reloaded
gnome-extensions install https://extensions.gnome.org/extension-data/trayIconsReloadedselfmade.pl.v29.shell-extension.zip

# install pano
gnome-extensions install https://extensions.gnome.org/extension-data/panoelhan.io.v22.shell-extension.zip

# install control blur effect
#gnome-extensions install https://extensions.gnome.org/extension-data/ControlBlurEffectOnLockScreenpratap.fastmail.fm.v25.shell-extension.zip

# install transparent top bar
gnome-extensions install https://extensions.gnome.org/extension-data/transparent-top-barftpix.com.v19.shell-extension.zip

# install ui improvements
gnome-extensions install https://extensions.gnome.org/extension-data/gnome-ui-tuneitstime.tech.v20.shell-extension.zip

# install wiggle
#gnome-extensions install https://extensions.gnome.org/extension-data/wigglemechtifs.v2.shell-extension.zip

# install dash to dock
#gnome-extensions install https://extensions.gnome.org/extension-data/dash-to-dockmicxgx.gmail.com.v89.shell-extension.zip

# install blur my shell
#gnome-extensions install https://extensions.gnome.org/extension-data/blur-my-shellaunetx.v55.shell-extension.zip
