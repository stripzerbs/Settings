#!/bin/bash

# clone pop-shell repo
git clone -b master_mantic https://github.com/pop-os/shell.git

# workdir
cd shell

# build and install

make && {
  make local-install
  cd ..
  rm -rf shell
  

} || {
  echo "failure to build shell"
}



