#!/bin/bash

curl -o jetbrains-toolbox.tar.gz https://download-cdn.jetbrains.com/toolbox/jetbrains-toolbox-2.2.3.20090.tar.gz

tar -zxvf jetbrains-toolbox.tar.gz

./jetbrains-toolbox-2.2.3.20090/jetbrains-toolbox

rm -rf jetbrains-toolbox-2.2.3.20090
rm jetbrains-toolbox.tar.gz

