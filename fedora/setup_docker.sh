#!/bin/bash

# install dependencies
sudo dnf install -y dnf-plugins-core

# config repos
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

# remove defaults
sudo dnf remove -y docker \
		docker-client \
		docker-client-latest \
		docker-common \
		docker-latest \
		docker-latest-logrotate \
		docker-selinux \
		docker-engine-selinux \
		docker-engine

# install docker
sudo dnf install -y docker-ce \
		docker-ce-cli containerd.io \
		docker-buildx-plugin \
		docker-compose-plugin

# add user group
sudo usermod -aG docker "$USER"

# starting services
sudo systemctl enable docker
sudo systemctl start docker

