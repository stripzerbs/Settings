#!/bin/bash

# clone repo
git clone https://github.com/vinceliuice/WhiteSur-icon-theme.git

# workdir
cd WhiteSur-icon-theme

# install
bash ./install.sh  -a -b && {
  cd ..
  rm -rf WhiteSur-icon-theme
} || {
  echo "failure to install icons"
}
