#!/bin/bash

# enable elegos
sudo dnf copr -y enable elegos/gitflow

# fedora update
sudo dnf update -y

# remove software that I dont like
sudo dnf remove -y libreoffice*

# install dependencies
sudo dnf install -y xprop libgda libgda-sqlite libxkbcommon-devel mesa-libEGL-devel glib2-devel

# install gnome codecs
sudo dnf install -y gstreamer1-plugins-{base,good,bad-free-extras} espeak

# install gnome tools
sudo dnf install -y gnome-tweaks gnome-extensions-app

# install dev tools
sudo dnf install -y git gitflow golang nodejs yarnpkg rust just cargo typescript

# install dotnet dev
sudo dnf install -y dotnet-sdk-* dotnet-templates-*

# install java
sudo dnf install -y java-17-openjdk-devel java-21-openjdk-devel

# install low level dev tools
sudo dnf install -y fpp lazarus nasm xxd qemu gnucobol

# install libs for flutter dev
sudo dnf install -y clang cmake ninja-build pkgconf-pkg-config gtk3-devel lzma-sdk-devel

# install virtual machines
sudo dnf install -y virt-manager

# install jetbrains
bash ./setup_jetbrains.sh

# install docker
bash ./setup_docker.sh

# install oh-my-posh
bash ./setup_posh.sh

# install pop-shell
# bash ./setup_pop_shell.sh
sudo dnf install gnome-shell-extension-pop-shell
bash ./setup_pop_launcher.sh

# install sdkman
bash ./setup_sdkman.sh

# install gnome extensions
bash ./setup_extensions.sh

# install whitesur theme
#bash ./setup_whitesur_theme.sh
#bash ./setup_whitesur_icons.sh

# install cloud cmd
#sudo dnf install -y awscli2 azure-cli

# configure git
bash ./setup_git.sh

# install flatpak
sudo dnf install -y flatpak

# add flatpak source
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# update flatpak repos
sudo flatpak update -y

# flatpak tools
bash ./setup_flatpak.sh

# system76 cosmic
bash ./setup_cosmic.sh

echo "Setup completed"

read -v 1 -s -r -p "Press any key to restart"

echo "Restarting the System"

sudo reboot

