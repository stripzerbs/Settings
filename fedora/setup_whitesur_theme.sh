#!/bin/bash

# dependencies actions
mkdir ~/.themes

# clone repo
git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git --depth=1

# workdir
cd WhiteSur-gtk-theme

# build

bash ./install.sh -HD -c Dark -c Light -t all --darker --nord && {
  sudo flatpak override --filesystem=xdg-config/gtk-4.0
  sudo bash ./tweaks.sh -g 
  bash ./tweaks.sh -f -e  -F -d --nord
  cd ..
  rm -rf WhiteSur-gtk-theme
} || {
  echo "failure to install WhiteSur theme"
}
